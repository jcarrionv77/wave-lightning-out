
function forceInit() {
	force.init(config);
};

function forceLogin(key) {
	forceInit();
	force.login(function(success) {
		var oauth = force.getOauth();
		setupLightning();
	});	
}

var _lightningReady = false;

function setupLightning(callback) {
	var appName = config.loApp;
	var oauth = force.getOauth();


    if (!oauth) {
        alert("Please login to Salesforce.com first!");
        return;
    }

	if (_lightningReady) {
		
		if (typeof callback === "function") {
			callback();
		}
	} else {
	    // Transform the URL for Lightning
	    console.log("setupLightning ");

		var idDash = document.getElementById("idDashboard").value;
		var MiJson = {};
		MiJson.dashboardId = idDash;

		console.log("MiJson es " + idDash);
		console.log("idDash es " + MiJson);

		//{dashboardId: "0FK6A000000kvyjWAA"}

	    var url = oauth.instanceUrl.replace("my.salesforce", "lightning.force");

	    console.log("url " + url);
	    console.log("access_token " + oauth.access_token);

		$Lightning.use("wave:waveApp", 
			        function() { 
			        	document.getElementById("login").style.display = "none";     
             			$Lightning.createComponent("wave:waveDashboard", MiJson, "chatterFeed");
            		},
            		url ,
            		oauth.access_token);


	   
	}
}

function createChatterFeed(type, subjectId) {
    setupLightning(function() {
		$Lightning.createComponent("forceChatter:feed", {type: type, subjectId: subjectId}, "chatterFeed"); 
    });
}
